# auto-pxe

Automate pxe booting

## Getting started

This is a simple script that allows one to automate pxe-booting from a web interface. My simple web page send a POST 
to my pxe boot server on port 9876 (or whatever port you pick). Xinetd running on my PXE boot server is listening on 
the chosen port and forwards the request to my python script that handles the request and start the pxe process.

## Dependencies

- DHCP server 
- TFTP service
- Xinetd service
- Web server + webpage to initiate the process

# How it works
1. Web interface sends a POST request to port on PXE server
2. Xinetd services which is listening on the specified port forwards the request to a handler script
3. Handler script processes the request and initiates the pxe boot using target server IMM.
4. Post-processing is done separately using ansible.
