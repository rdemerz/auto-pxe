#!/bin/python3
import os
import sys
import subprocess
import shlex
import logging

#Change this to use an env variable
IMM_USERID: str = 'USERID'   #IMM USERNAME
IMM_PASSWD: str = 'PASSW0RD' #IMM PASSWORD

#maybe include more defaults
def snd_imm_cmd(ip: str, 
                user: str,
                passwd: str,
                cmd: str = 'power status'
                ):
    return subprocess.getoutput(f"ipmitool -H {ip} -I lanplus -U{user} -P{passwd} {cmd}")

logging.basicConfig(filename='pxe-boot.log', level=logging.DEBUG)

_, params, _ = input().split()  # Get input from website

host_info = dict([x.split('=') for x in params[2:].split('&')])
logging.info(f"Received request with the following parameters paramers: {host_info}")

for param in ['mac', 'hostname', 'imm_ip']:
    if param not in host_info:
        logging.info(f"{param} NOT FOUND. EXITING")
        sys.exit(f"{param} NOT PROVIDED")

config_name = "01-" + "-".join(host_info['mac'].lower().split('%3a'))
logging.info(f"{config_name} pointing to {host_info['os']}")

# creates a symlink to pxe config file in tftpboot dir.
try:
    os.symlink(
        f'/tftpboot/pxelinux.cfg/{host_info["os"]}_config',
        f'/tftpboot/pxelinux.cfg/{config_name}'
    )
except FileExistsError:
    os.unlink(f'/tftpboot/pxelinux.cfg/{config_name}')
    os.symlink(
        f'/tftpboot/pxelinux.cfg/{host_info["os"]}_config',
        f'/tftpboot/pxelinux.cfg/{config_name}'
    )

# Gets server's power status from imm, if not, tried an alternative password
status = snd_imm_cmd(
    host_info['imm_ip'],
    IMM_USERID,
    IMM_PASSWD,
    "power status")

if 'Error' in status:
    ALT_IMM_PASSWD = 'ALT_PASSWORD'     # Make sure to set your password here.
                                        # TODO: Implement reading credentials from env file.

    status = snd_imm_cmd(host_info['imm_ip'], IMM_USERID, ALT_IMM_PASSWD, "power status")
    if 'Error' in status:
        logging.info("Failed to login into IMM")
        sys.exit("Couldn't login into IMM")

snd_imm_cmd(host_info['imm_ip'], IMM_USERID, IMM_PASSWD, "chassis bootdev pxe")

if 'off' in status:
    snd_imm_cmd(host_info['imm_ip'], IMM_USERID, IMM_PASSWD, "power on")
else:
    snd_imm_cmd(host_info['imm_ip'], IMM_USERID, IMM_PASSWD, "power cycle")

print("HTTP/1.1 200 OK\n\n ALL GOOD, OS INSTALLATION STARTING!")
